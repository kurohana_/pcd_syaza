function varargout = uji(varargin)
% UJI MATLAB code for uji.fig
%      UJI, by itself, creates a new UJI or raises the existing
%      singleton*.
%
%      H = UJI returns the handle to a new UJI or the handle to
%      the existing singleton*.
%
%      UJI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UJI.M with the given input arguments.
%
%      UJI('Property','Value',...) creates a new UJI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before uji_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to uji_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help uji

% Last Modified by GUIDE v2.5 25-Mar-2020 20:25:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @uji_OpeningFcn, ...
                   'gui_OutputFcn',  @uji_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before uji is made visible.
function uji_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to uji (see VARARGIN)

% Choose default command line output for uji
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes uji wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = uji_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName, PathName] = uigetfile('*.bmp;*.jpg;*.png', 'Pilih Gambar');
fullpathname = strcat(PathName, FileName);
I = imread(fullpathname);
axes(handles.axes1);
imshow(I);
[p, l, t] = size(I); %mengitung ukuran gambar
I = I((p/4):(3*p/4),(l/4):(3*l/4),:);%memotong gambar menjadi kotak (center)
axes(handles.axes2);
imshow(I);
I = imresize(I, [100 100]); %mengubah ukuran gambar menjadi 100x100
axes(handles.axes3);
imshow(I);
data = EkstraksiCiri(I);
allBlack = zeros(size(I, 1), size(I, 2), 'uint8');
[R, G, B, H, S, V]=Morfologi(I);
bentuk = im2bw(S,.5);
bentuk = bwareaopen(bentuk,50);
axes(handles.axes4);
imshow(bentuk);
axes(handles.axes5);
imshow(cat(3,R,G,B));
axes(handles.axes6);
imshow(cat(3, R, allBlack, allBlack));
axes(handles.axes7);
imshow(cat(3, allBlack, G, allBlack));
axes(handles.axes8);
imshow(cat(3, allBlack, allBlack, B));
[H, S, V] = HSV(cat(3,R,G,B));
axes(handles.axes9);
imshow(H);
axes(handles.axes10);
imshow(S);
axes(handles.axes11);
imshow(V);
load dbmodel;
load dblabel;
output = sim(net,data');
if output == 0
    set(handles.hasil, 'String', label(1,:));
else
    set(handles.hasil, 'String', label(2,:));
end