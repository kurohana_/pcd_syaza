function testing
    clc;
    load dbmodel;
    images_folder = uigetdir(path); %memilih folder dataset
    dataset_folder = dir(images_folder); %membaca isi folder dataset
    [m, n] = size(dataset_folder); %menghitung jumlah isi folder dataset
    j = 1;
    data = [];
    target = [];
    for class = 0:m-3 %melakukan looping sejumlah isi folder dataset
        folder_target = images_folder + "\" + dataset_folder(class+3).name; %membuat folder target sesuai folder dalam folder dataset
        images = dir(fullfile(folder_target,'*.jpg')); %mengambil semua gambar berformat .jpg pada folder target
        total_images = numel(images); %menghitung jumlah gambar yang terambil pada folder target
        for n = 1:total_images %melakukan looping sebanyak jumlah gambar
            image = fullfile(folder_target,images(n).name); %membaca nama file gambar
            I = imread(convertStringsToChars(image)); %membaca / memuat gambar
            [p, l, t] = size(I); %mengitung ukuran gambar
            I = I((p/4):(3*p/4),(l/4):(3*l/4),:);%memotong gambar menjadi kotak (center)
            I = imresize(I, [100 100]); %mengubah ukuran gambar menjadi 100x100
            F = EkstraksiCiri(I);
            data = [data; F];
            target = [target class];
            j = j+1;
        end
    end
    output = sim(net,data');
    hasil = [target' output'];
    save dbtesting.mat hasil;
    if isfile('ConfusionMatrix.png')
        delete ConfusionMatrix.png
    end
end