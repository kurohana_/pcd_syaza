function varargout = evaluasi(varargin)
% EVALUASI MATLAB code for evaluasi.fig
%      EVALUASI, by itself, creates a new EVALUASI or raises the existing
%      singleton*.
%
%      H = EVALUASI returns the handle to a new EVALUASI or the handle to
%      the existing singleton*.
%
%      EVALUASI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EVALUASI.M with the given input arguments.
%
%      EVALUASI('Property','Value',...) creates a new EVALUASI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before evaluasi_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to evaluasi_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help evaluasi

% Last Modified by GUIDE v2.5 25-Mar-2020 19:06:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @evaluasi_OpeningFcn, ...
                   'gui_OutputFcn',  @evaluasi_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before evaluasi is made visible.
function evaluasi_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to evaluasi (see VARARGIN)

% Choose default command line output for evaluasi
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes evaluasi wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = evaluasi_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load dbtesting
TP = 0; TN = 0; FP = 0; FN = 0; P = 0; N = 0; 
[m, n] = size(hasil);
for i = 1:m
    if hasil(i,1) == 0
        P = P+1;
        if hasil(i,1) == hasil(i,2)
            TP = TP+1;
        else
            FN = FN+1;
        end
    else
        N = N+1;
        if hasil(i,1) == hasil(i,2)
            TN = TN+1;
        else
            FP = FP+1;
        end
    end
end
acc = (TP+TN)/(P+N);
rec = TP/P;
spe = TN/N;
pre = TP/(TP+FP);
set(handles.acc, 'String', join(['Accuracy : ',num2str(acc)]));
set(handles.rec, 'String', join(['Recal : ',num2str(rec)]));
set(handles.spe, 'String', join(['Specificity : ',num2str(spe)]));
set(handles.pre, 'String', join(['Precision : ',num2str(pre)]));