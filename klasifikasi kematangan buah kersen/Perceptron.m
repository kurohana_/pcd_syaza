function [output, net]=Perceptron(data,target)
    net = newp(data',target);
    net.trainParam.epochs = 100;
    net = train(net,data',target);
    output = sim(net,data');
end