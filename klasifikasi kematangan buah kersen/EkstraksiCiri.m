function [F]=EktraksiCiri(I)
    [R, G, B, H, S, V]=Morfologi(I);
    [H, S, V] = HSV(cat(3,R,G,B));
    R = mean2(R);
    G = mean2(G);
    B = mean2(B);
    H = mean2(H);
    S = mean2(S);
    V = mean2(V);
    F = [R, G, B, H, S, V];
end