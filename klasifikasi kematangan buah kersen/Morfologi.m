function [R, G, B, H, S, V]=Morfologi(I)
    [H, S, V] = HSV(I);
    bentuk = im2bw(S,.5);
    bentuk = bwareaopen(bentuk,50);
    R = I(:,:,1);
    G = I(:,:,2);
    B = I(:,:,3);
    R(~bentuk) = 0;
    B(~bentuk) = 0;
    G(~bentuk) = 0;
    %bentuk = cat(3,R,G,B);
end