function [acc, rec, spe, pre]=Evaluasi(hasil)
    TP = 0; TN = 0; FP = 0; FN = 0; P = 0; N = 0; 
    [m, n] = size(hasil);
    for i = 1:m
        if hasil(i,1) == 0
            P = P+1;
            if hasil(i,1) == hasil(i,2)
                TP = TP+1;
            else
                FN = FN+1;
            end
        else
            N = N+1;
            if hasil(i,1) == hasil(i,2)
                TN = TN+1;
            else
                FP = FP+1;
            end
        end
    end
    acc = (TP+TN)/(P+N);
    rec = TP/P;
    spe = TN/N;
    pre = TP/(TP+FP);
end