function [net]=Perceptron(data,target)
    net = newff(data',target,[10 5],{'logsig','logsig'},'trainlm');
    net.trainParam.epochs = 100;
    net.trainParam.goal = 1e-5;
    net = train(net,data',target);
end