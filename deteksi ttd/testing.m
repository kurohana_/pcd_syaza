function [hasil]=Testing(net)
    [data, target]=Ekstraksi;
    output = round(sim(net,data'));
    hasil = [target' output'];
end