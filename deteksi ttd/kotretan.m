%Kode load gambar
i = imread('E:\syaza\pcd_syaza\deteksi ttd\dataset\a\a1.jpg');
imshow(i)

%Kode memotong gambar menjadi kotak
[p, l, t] = size(i);
i = i((p-l)/2:((p-l)/2)+l,:,:);
imshow(i)

%Kode compress gambar ke ukuran 60x60
i = imresize(i, [60 60]);
imshow(i)

%Kode mengubah citra RGB menjadi citra biner
i = im2bw(i,.9);
imshow(i)

%Kode segmentasi vertical grid
vg1 = i(1:20,:);
vg2 = i(21:40,:);
vg3 = i(41:60,:);
subplot(3,1,1);
title('Bagian atas')
imshow(vg1)
subplot(3,1,2);
title('Bagian tengah')
imshow(vg2)
subplot(3,1,3);
title('Bagian bawah')
imshow(vg3)

%Kode mencari nilai2 statistik pada vertical grid yang dihasilkan
hasilvg=[]
for x = 1:3
    m = mean2(i((20*(x-1))+1:20*x,:));
    st = std2(i((20*(x-1))+1:20*x,:));
    s = sum(sum(~i((20*(i-1))+1:20*x,:)));
    hasilvg = [hasilvg; [m, st, s]];
end
disp(hasilvg)

%Kode segmentasi horizontal grid
hg1 = i(:,1:20);
hg2 = i(:,21:40);
hg3 = i(:,41:60);
subplot(1,3,1);
title('Bagian kiri')
imshow(hg1)
subplot(1,3,2);
title('Bagian tengah')
imshow(hg2)
subplot(1,3,3);
title('Bagian kanan')
imshow(hg3)

%Kode mencari nilai2 statistik pada horizontal grid yang dihasilkan
hasilhg = []
for x = 1:3
    m = mean2(i(:,(20*(x-1))+1:20*x));
    st = std2(i(:,(20*(x-1))+1:20*x));
    s = sum(sum(~i(:,(20*(x-1))+1:20*x,:)));
    hasilhg = [hasilhg; [m, st, s]];
end
disp(hasilhg)