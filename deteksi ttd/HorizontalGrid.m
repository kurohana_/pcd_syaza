function [hasilvg]=HorizontalGrid(i)
    hasilvg=[];
    for x = 1:3
        hasil = FeatureStatistical(i(:,(20*(x-1))+1:20*x));
        hasilvg = [hasilvg hasil];
    end
end