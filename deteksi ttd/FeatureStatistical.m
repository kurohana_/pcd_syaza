function [hasil]=FeatureStatistical(im)
    m = mean2(im); %menghitung nilai rata-rata
    st = std2(im); %menghitung standar deviasi
    s = sum(sum(~im));
    hasil = [m st s];
end

